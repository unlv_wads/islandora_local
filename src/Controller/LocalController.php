<?php

namespace Drupal\islandora_local\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\islandora\IslandoraUtils;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Local Controllers.
 *
 * @package Drupal\islandora_local\Controller
 */
class LocalController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Islandora utilities.
   *
   * @var \Drupal\islandora\IslandoraUtils
   */
  protected $islandoraUtils;

  /**
   * Constructs a LocalController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\islandora\IslandoraUtils $islandora_utils
   *   Islandora utility class.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    IslandoraUtils $islandora_utils
  ) {
    $this->islandoraUtils = $islandora_utils;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('islandora.utils')
    );
  }

  /**
   * Returns a thumbnail for the given node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The Node you want the thumbnail for.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   200 on success with the thumbnail.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function getNodeThumbnail(NodeInterface $node, Request $request) {
    try {
      $thumbnail_term = $this->islandoraUtils->getTermForUri('http://pcdm.org/use#ThumbnailImage');
      $thumbnail_media = $this->islandoraUtils->getMediaWithTerm($node, $thumbnail_term);
      if (is_null($thumbnail_media)) {
        throw new NotFoundHttpException("No thumbnail media entity was found for node " . $node->id());
      }
      $thumbnail_file = $thumbnail_media->field_media_image->entity;
      if (is_null($thumbnail_file)) {
        throw new NotFoundHttpException("No thumbnail image file entity was found for node " . $node->id());
      }
      return new BinaryFileResponse($thumbnail_file->getFileUri());
    }
    catch (Exception $e) {
      throw new NotFoundHttpException();
    }
  }

}
