<?php

namespace Drupal\islandora_local\Controller;

use Drupal\system\Controller\EntityAutocompleteController as OriginalController;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\islandora_local\EntityAutocompleteMatcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Autocomplete Controller.
 */
class EntityAutocompleteController extends OriginalController {

  /**
   * The autocomplete matcher for entity references.
   *
   * @var Drupal\islandora_local\EntityAutocompleteMatcher
   */
  protected $matcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityAutocompleteMatcher $matcher, KeyValueStoreInterface $key_value) {
    $this->matcher = $matcher;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('islandora_local.autocomplete_matcher'),
      $container->get('keyvalue')->get('entity_autocomplete')
    );
  }

}
