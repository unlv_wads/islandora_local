<?php

namespace Drupal\islandora_local\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Local route subscriber.
 */
class IslandoraLocalRouteSubscriber extends RouteSubscriberBase {

  /**
   * Alter core routes.
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.entity_autocomplete')) {
      $route->setDefault('_controller', 'Drupal\islandora_local\Controller\EntityAutocompleteController::handleAutocomplete');
    }
  }

}
