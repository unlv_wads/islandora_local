<?php

namespace Drupal\islandora_local\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a processor for TransformMedia.
 *
 * @FacetsProcessor(
 *   id = "content_type_transform",
 *   label = @Translation("Content Type Transform"),
 *   description = @Translation("Maps Content Types to end-user friendly labels."),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class ContentTypeTransform extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $config = $this->getConfiguration();

    // @todo Use a config file for the map instead of hard-coding them.

    /** @var \Drupal\facets\Result\Result $result */
    foreach ($results as $result) {
      switch ($result->getRawValue()) {
        case 'dc_object':
          $result->setDisplayValue(t('Digital Object'));
          break;

        case 'archival_object':
          $result->setDisplayValue(t('Archival Record'));
          break;

        case 'archival_resource':
          $result->setDisplayValue(t('Archival Collection'));
          break;

        case 'islandora_object':
          $result->setDisplayValue(t('Digital Object'));
          break;
      }
    }

    return $results;
  }

}
