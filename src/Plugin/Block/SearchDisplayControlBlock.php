<?php

namespace Drupal\islandora_local\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a search page results control block.
 *
 * @Block(
 *   id = "search_display_block",
 *   admin_label = @Translation("Search display control block"),
 *   category = @Translation("Search")
 * )
 */
class SearchDisplayControlBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $params = \Drupal::request()->query->all();

    // Make search term buttons.
    $term_buttons = [];
    $search = (array_key_exists('keys', $params)) ? $params['keys'] : '';
    if (!empty($search)) {
      $terms = str_getcsv($search, ' ', '"');
      $quote_spaced = function ($term) {
        return (strpos($term, ' ') !== FALSE) ? '"' . $term . '"' : $term;
      };
      foreach ($terms as $term) {
        $button_query = $params;
        $button_query['keys'] = implode(' ', array_map($quote_spaced, array_diff($terms, [$term])));
        $term_button = Url::fromRoute('<current>');
        $term_button->setOptions(['query' => $button_query]);
        $term_buttons[] = Link::fromTextAndUrl($term, $term_button)->toRenderable();
      }
    }

    $facet_buttons = [];
    if (array_key_exists('f', $params)) {
      $facets = $params['f'];
      if (!empty($facets)) {
        foreach (array_reverse($facets) as $facet) {
          $button_query = $params;
          $button_query['f'] = array_diff($facets, [$facet]);
          $facet_button = Url::fromRoute('<current>');
          $facet_button->setOptions(['query' => $button_query]);
          $facet_text = substr($facet, strpos($facet, ':') + 1);

          // Make integer range facet pretty.
          $matches = [];
          if (preg_match('#\(min:(\d+),max:(\d+)\)#', $facet_text, $matches)) {
            $facet_text = $matches[1] . " - " . $matches[2];
          }

          $facet_buttons[] = Link::fromTextAndUrl($facet_text, $facet_button)->toRenderable();
        }
      }
    }

    // Pagination Form support.
    $paginations = [];
    $items_per_page = array_key_exists('items_per_page', $params) ? $params['items_per_page'] : '';
    foreach ([10, 25, 50, 100, 250] as $count) {
      $option = [
        'label' => $count,
        'value' => $count,
      ];
      $option['selected'] = ($count == $items_per_page);
      $paginations[] = $option;
    }
    // Unset values we don't want in the pagination dropdown form.
    unset($params['items_per_page']);
    unset($params['page']);
    // Facets nest in Arrays, but we want the list flat.
    $collapsed_params = [];
    foreach ($params as $param => $value) {
      if (is_array($value)) {
        foreach ($value as $i => $nested_value) {
          $collapsed_params[$param . "[$i]"] = $nested_value;
        }
      }
      else {
        $collapsed_params[$param] = $value;
      }
    }

    // Now send the template what it needs.
    $build = [
      '#theme' => 'search_display_control_block',
      '#cache' => [
        'max-age' => 0,
      ],
      '#sorts' => [],
      '#paginations' => $paginations,
      '#existing_params' => $collapsed_params,
      '#term_buttons' => $term_buttons,
      '#facet_buttons' => $facet_buttons,
    ];

    return $build;
  }

}
