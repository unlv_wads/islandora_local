<?php

namespace Drupal\islandora_local\Plugin\Field\FieldFormatter;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'ArchivalPhysicalInstanceFormatter'.
 *
 * @FieldFormatter(
 *   id = "as_physical_instance_container_indicator",
 *   label = @Translation("Container Indicator Formatter"),
 *   field_types = {
 *     "archival_physical_instance"
 *   }
 * )
 */
class InstanceContainerIndicatorFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      // This is teriibly brittle and not at all customizable, but it works for now. 😬.
      $text = "";
      $text = FieldStorageConfig::loadByName('node', 'field_as_container_type')->get('settings')['allowed_values'][$item->entity->field_as_container_type->get(0)->value];
      $text .= ' ' . $item->entity->field_as_container_indicator->value;

      // Prepend a statement about being restricted (if applicable).
      if ($item->entity->field_restrictions_bool->value) {
        $text .= ' (' . $this->t('Restrictions apply') . ')';
      }

      $elements[$delta] = ['#plain_text' => $text];
    }
    return $elements;
  }

}
