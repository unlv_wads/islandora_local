<?php

namespace Drupal\islandora_local\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'ASDateFormatter'.
 *
 * @todo Use more of the available fields.
 *
 * @FieldFormatter(
 *   id = "local_as_date_edtf",
 *   label = @Translation("ArchivesSpace Date EDTF Formatter"),
 *   field_types = {
 *     "as_date"
 *   }
 * )
 */
class ASEDTFFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $date_span_separator = ' to ';
    // @todo Clean this up, simply a WIP proof-of-concept.
    $output = [];
    foreach ($items as $delta => $item) {
      $certainty = ($item->certainty && $item->certainty == 'approximate') ? '~' : '';
      $display_value = '';
      if (!empty($item->begin)) {
        $display_value .= $item->begin . $certainty;
      }
      if (!empty($item->end)) {
        $display_value .= (empty($display_value)) ? '../' : '/';
        $display_value .= $item->end . $certainty;
      }
      if (empty($display_value)) {
        $display_value = $item->expression;
      }
      $output[$delta] = ['#plain_text' => $display_value];
    }

    return $output;
  }

}
