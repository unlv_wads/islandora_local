<?php

namespace Drupal\islandora_local\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\link\LinkItemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'LinkedEntityLink'.
 *
 * @FieldFormatter(
 *   id = "linked_entity_link",
 *   label = @Translation("Linked Entity Link"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class LinkedEntityLink extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $link_fields = [];
    foreach (\Drupal::service('entity_field.manager')->getFieldMap() as $entity_type => $fields) {
      foreach ($fields as $field_name => $field_info) {
        if ($field_info['type'] === 'link') {
          $link_fields[$field_name] = $field_name;
        }
      }
    }
    $element['link_field'] = [
      '#type' => 'select',
      '#title' => t('Link Field'),
      '#options' => $link_fields,
      '#required' => TRUE,
      '#default_value' => $settings['link_field'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $target) {
      try {
        $field_item_list = $target->get($this->getSetting('link_field'));

        if ($field_item_list->count() > 0) {
          foreach ($field_item_list as $delta2 => $item) {
            $link_title = $target->label();
            $url = $this->buildUrl($item);
            $element[$delta] = [
              '#type' => 'link',
              '#title' => $link_title,
            ];
            $element[$delta]['#url'] = $url;
            if (!empty($item->_attributes)) {
              $element[$delta]['#options'] += [
                'attributes' => [],
              ];
              $element[$delta]['#options']['attributes'] += $item->_attributes;

              // Unset field item attributes since they have been included in the
              // formatter output and should not be rendered in the field template.
              unset($item->_attributes);
            }
          }
        }
        else {
          $element[$delta] = $target->toLink()->toRenderable();
        }
      }
      catch (\InvalidArgumentException $e) {
        \Drupal::logger('islandora_local')->warning('LinkedEntityLink attempted to use an invalid field: ' . $e->getMessage());
        $element[$delta] = $target->toLink()->toRenderable();
      }
    }

    return $element;

  }

  /**
   * Builds the \Drupal\Core\Url object for a link field item.
   *
   * Stolen from Drupal\link\Plugin\Field\FieldFormatter, which we don't extend.
   *
   * @param \Drupal\link\LinkItemInterface $item
   *   The link field item being rendered.
   *
   * @return \Drupal\Core\Url
   *   A Url object.
   */
  protected function buildUrl(LinkItemInterface $item) {
    $url = $item
      ->getUrl() ?: Url::fromRoute('<none>');
    $options = $item->options;
    $options += $url
      ->getOptions();

    $url
      ->setOptions($options);
    return $url;
  }

}
