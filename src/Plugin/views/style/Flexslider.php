<?php

namespace Drupal\islandora_local\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Custom plugin to render divs for FlexSlider
 * complex object displays.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *  id = "flexslider",
 *  title = @Translation("FlexSlider"),
 *  theme = "views_view_flexslider",
 *  display_types = { "normal" }
 * )
 */
class Flexslider extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

}
