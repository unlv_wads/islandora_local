<?php

namespace Drupal\islandora_local\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Renders the breadcrumbs of a node in a field.
 *
 * @todo Add configurable delimiter
 * @todo Add configurable fields
 * @todo Add configurable include-self.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("local_breadcrumbs_views_field")
 */
class BreadcrumbsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;
    $utils = \Drupal::service('islandora.utils');
    $ancestors = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($utils->findAncestors($node, ['field_member_of']));

    $breadcrumbs = implode(' > ', array_map(fn($entity): string => $entity->label(), array_reverse($ancestors)));
    return $breadcrumbs;
  }

}
