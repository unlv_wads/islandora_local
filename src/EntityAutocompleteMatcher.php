<?php

namespace Drupal\islandora_local;

use Drupal\Core\Entity\EntityAutocompleteMatcher as OriginalMatcher;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;

/**
 * Provides URIs for terms and published status.
 */
class EntityAutocompleteMatcher extends OriginalMatcher {

  /**
   * {@inheritdoc}
   */
  public function getMatches($target_id, $selection_handler, $selection_settings, $string = '') {
    $matches = [];
    if (!isset($string)) {
      return $matches;
    }
    // Get the matches with different limits based on type of referenced entity.
    $handler = $this->selectionManager->getInstance($selection_settings + [
      'target_type' => $target_id,
      'handler' => $selection_handler,
    ]);
    $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
    $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 20;
    $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $match_limit);
    // Customize the labels used in autocomplete.
    foreach ($entity_labels as $values) {
      foreach ($values as $entity_id => $label) {
        if ($target_id == 'taxonomy_term') {
          $custom_label = $this->getLabelForTerm($entity_id, $target_id, $label);
        }
        else {
          $custom_label = $this->getLabel($entity_id, $target_id, $label);
        }
        // Create a sanitized key.
        $key = "{$label} ({$entity_id})";
        $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
        $key = Tags::encode($key);
        $matches[] = ['value' => $key, 'label' => $custom_label];
      }
    }
    return $matches;
  }

  /**
   * Add URI to terms.
   */
  protected function getLabelForTerm($entity_id, $entity_type_id, $label) {
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $entity = $term_storage->load($entity_id);
    // URI; yes, we are hard-coding it for my sanity...
    if (!empty($entity->field_authority_link->get(0)->uri)) {
      $label = $label . ' -- ' . $entity->field_authority_link->get(0)->uri;
    }
    // Published?
    if ($entity instanceof EntityPublishedInterface && !$entity->isPublished()) {
      $label = $label . ' [Unpublished]';
    }
    return $label;
  }

  /**
   * Adds published status.
   */
  protected function getLabel($entity_id, $entity_type_id, $label) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    if ($entity instanceof EntityPublishedInterface && !$entity->isPublished()) {
      $label = $label . ' [Unpublished]';
    }
    return $label;
  }

}
