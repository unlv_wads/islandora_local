<?php

/**
 * @file
 * This script attempts to identify taxonomy terms with similar
 * labels using a levenshtein distance of their metaphones so
 * they can be manually reviewed for possible duplicates.
 */

date_default_timezone_set('America/Los_Angeles');
$database = \Drupal::database();
$term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

$migrated_terms = [];
$q = $database->select("migrate_map_as_subjects", 'mm');
$q->fields('mm', ['destid1', 'sourceid1'])->condition('mm.destid1', 0, '>');
$results = $q->execute();
foreach ($results as $result) {
  $migrated_terms[$result->destid1] = $result->sourceid1;
}

$metaphones = [];
$terms = [];
// Columns = Name, DAMS path, VID, AS URI, METAPHONE, Highest levenshtein %.
$q = $database->select('taxonomy_term_field_data', 'ttfd');
$q->fields('ttfd', ['tid'])
  ->condition('ttfd.vid', ['material_type', 'subject', 'geo_location'], 'IN');
foreach ($q->execute()->fetchCol() as $tid) {
  $term = $term_storage->load($tid);
  if (!empty($term)) {
    $metaphone = metaphone($term->label(), 250);
    $metaphones[$metaphone][] = $term->id();
    $uri = ($term->hasField('field_authority_link') && !empty($term->field_authority_link->uri)) ? $term->field_authority_link->uri : '';
    $note = (empty($term->description->value)) ? '' : strip_tags(str_replace(["\n", "\r"], '', $term->description->value), '<p><a>');
    $as_uri = (array_key_exists($term->id(), $migrated_terms)) ? $migrated_terms[$term->id()] : '';
    $terms[$term->id()] = [
      'name' => $term->label(),
      'path' => '/taxonomy/term/' . $term->id(),
      'vid' => $term->bundle(),
      'as_uri' => $as_uri,
      'metaphone' => $metaphone,
      'levenshtein' => 100,
      'most_likely_match' => '',
      'note' => $note,
    ];
  }
}

foreach ($metaphones as $metaphone => $tids) {

  if (count($tids) > 1) {
    foreach ($tids as $tid) {
      $terms[$tid]['levenshtein'] = 0;
      foreach ($tids as $tid2) {
        if ($tid != $tid2) {
          $terms[$tid]['most_likely_match'] = $terms[$tid2]['path'];
          break;
        }
      }
    }
  }
  else {
    $tid = $tids[0];
    foreach ($metaphones as $comparitor => $comp_tids) {
      if ($metaphone == $comparitor) {
        continue;
      }
      $error_rate = levenshtein($metaphone, $comparitor) / strlen($metaphone);
      if ($error_rate < $terms[$tid]['levenshtein']) {
        $terms[$tid]['levenshtein'] = $error_rate;
        $terms[$tid]['most_likely_match'] = $terms[$comp_tids[0]]['path'];
      }
    }
  }
}

foreach ($terms as $term) {
  print(implode("\t", [
    $term['name'],
    $term['path'],
    $term['vid'],
    $term['as_uri'],
    $term['note'],
    $term['metaphone'],
    number_format($term['levenshtein'], 2, '.', ''), $term['most_likely_match'],
  ]) . "\n");
}
