<?php

/**
 * @file
 */

$database = \Drupal::database();
$term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
foreach (['people', 'corp', 'family'] as $group) {
  $q = $database->select("migrate_map_as_agents_$group", 'mm');
  $q->fields('mm', ['destid1', 'sourceid1'])->condition('mm.destid1', 0, '>');
  $results = $q->execute();
  foreach ($results as $result) {
    $term = $term_storage->load($result->destid1);
    if (empty($term)) {
      print("Could not load " . $result->destid1 . " (" . $result->sourceid1 . ")\n");
    }
    elseif ($term->hasField('field_authority_link') && !empty($term->field_authority_link->uri)) {
      print("Skipping " . $term->label() . "\n");
    }
    else {
      print("/taxonomy/term/" . $term->id() . "\t" . $term->label() . "\t" . $result->sourceid1 . "\n");
    }
  }
}
