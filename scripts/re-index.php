<?php

/**
 * @file
 * ActiveMQ and karaf weren't working so records weren't being indexed.
 *
 * This script, by updating the `$change_epoc` allows us to re-index
 * all the items that were changed since the indexing failed.
 */

$change_epoc = 1615161600;
$accountSwitcher = \Drupal::service('account_switcher');
$account = \Drupal::entityTypeManager()->getStorage('user')->load(3);
$accountSwitcher->switchTo($account);
$index_in_fedora = \Drupal::entityTypeManager()->getStorage('action')->load('index_node_in_fedora');
$index_in_triplestore = \Drupal::entityTypeManager()->getStorage('action')->load('index_node_in_triplestore');
$node_storage = \Drupal::entityTypeManager()->getStorage('node');
foreach (\Drupal::entityQuery('node')->condition('changed', $change_epoc, ">")->execute() as $nid) {
  $node = $node_storage->load($nid);
  print(implode("\t", [
    "INDEXING",
    $node->id(),
    $node->label(),
    $node->field_digital_id->value,
  ]) . "\n"
  );
  $index_in_fedora->execute([$node]);
  $index_in_triplestore->execute([$node]);
}
