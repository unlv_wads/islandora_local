<?php

/**
 * @file
 * In cases where we accidentally re-used ARKs that were then
 * used for aliases, fixing the ARKs resulted in old alias
 * entries that were causing the wrong item to appear.
 *
 * This script identifies aliases that don't match the current
 * one (incorrect ARKs) and removes them.
 */

$query = \Drupal::entityQuery('node');
$query->condition('type', 'dc_object', '=');
$nids = $query->execute();
$path_alias_storage = \Drupal::entityTypeManager()->getStorage('path_alias');

foreach ($nids as $nid) {
  $actual_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $nid);
  print("Checking node/$nid ($actual_alias)\n");
  // Load all path alias for this node.
  $alias_objects = $path_alias_storage->loadByProperties([
    'path' => '/node/' . $nid,
  ]);

  // Delete all other alias than the actual one.
  foreach ($alias_objects as $alias_object) {
    if ($alias_object->get('alias')->value !== $actual_alias) {
      print("\tDeleting bad alias " . $alias_object->get('alias')->value . "\n");
      $alias_object->delete();
    }
  }
  // Load all new path alias for this node.
  $new_alias_objects = $path_alias_storage->loadByProperties([
    'path' => '/node/' . $nid,
  ]);
  // Delete duplicate aliases.
  if (count($new_alias_objects) > 1) {
    array_shift($new_alias_objects);
    foreach ($new_alias_objects as $alias_object) {
      $alias_object->delete();
    }
  }
}
