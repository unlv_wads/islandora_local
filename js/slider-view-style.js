$(window).on('load',function () {
  $('.flexslider-view-style').flexslider({
    animation: "slide",
    controlNav: "thumbnails",
    smoothHeight: true,
    slideshow: false,
  });
  $('.flex-control-thumbs li:nth-child(6n):visible').after("<div class='clear'></div>");
});

$(function () {
  $(".customjs").hide().first().show();
  $(".sidenav li:first").addClass("ui-tabs-active");

  $(".sidenav a").on('click', function (e) {
    e.preventDefault();
    $(this).closest('li').addClass("ui-tabs-active").siblings().removeClass("ui-tabs-active");
    $($(this).attr('href')).show().siblings('.customjs').hide();
  });

  var hash = $.trim( window.location.hash );

  if (hash) { $('.sidenav a[href$="' + hash + '"]').trigger('click');
  }

});
